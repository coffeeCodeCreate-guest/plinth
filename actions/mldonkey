#!/usr/bin/python3
# -*- mode: python -*-
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for mldonkey.
"""

import argparse
import subprocess

from plinth import action_utils


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparsers.add_parser('pre-install', help='Perform pre-install operations')
    subparsers.add_parser('enable', help='Enable mldonkey')
    subparsers.add_parser('disable', help='Disable mldonkey')

    subparsers.required = True
    return parser.parse_args()


def subcommand_pre_install(_):
    """Preseed debconf values before packages are installed."""
    subprocess.check_output([
        'debconf-set-selections'
    ], input=b'mldonkey-server mldonkey-server/launch_at_startup boolean true')


def subcommand_enable(_):
    """Enable web configuration and reload."""
    action_utils.service_enable('mldonkey-server')
    action_utils.webserver_enable('mldonkey-freedombox')


def subcommand_disable(_):
    """Disable web configuration and reload."""
    action_utils.webserver_disable('mldonkey-freedombox')
    action_utils.service_disable('mldonkey-server')


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
